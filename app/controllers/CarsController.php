<?php

// app/controllers/CarsController.php

class CarsController extends BaseController
{
    public function index($column = 'name', $sorttype = 'asc')
    {
		
		$carss = DB::table('cars')
            ->join('drivers', 'drivers.id', '=', 'cars.driver_id')
            ->select('drivers.name', 'drivers.surname', 'cars.registrationNumber', 'cars.dateOilChange', 'cars.dateService', 'cars.id')
			->orderBy($column, $sorttype)
            ->get();
		$cars = json_decode(json_encode($carss), true);
		
		return View::make('car.index')->with('cars', $cars);
    }

    public function create()
    {
		$drivers = Driver::all();
        return View::make('car.create')->with('drivers', $drivers);
    }

    public function handleCreate()
    {
		$rules = array(	
			'registrationNumber'  => 'required|unique:cars'
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
		
			$messages = $validator->messages();
			return Redirect::to('cars/create')->withErrors($validator);
			
		} else {
		
			DB::table('cars')->insert(
				array(
						'driver_id' => Input::get('driver_id'),
						'registrationNumber' => Input::get('registrationNumber'),
						'dateOilChange' => Input::get('dateOilChange'),
						'dateService' => Input::get('dateService')
					)
			);
			return Redirect::action('CarsController@index');
		}
    }

    public function edit($id)
    {
		$drivers = Driver::all();
		$cars = DB::table('cars')->where('id', $id)->first();
		return View::make('car.edit')->with('car', $cars)->with('drivers', $drivers);
    }

    public function handleEdit()
    {
		$rules = array(	
			'registrationNumber'  => 'required|unique:cars'
		);
		
		$validator = Validator::make(Input::all(), $rules);
		$car = DB::table('cars')->where('id', Input::get('id'))->where('registrationNumber', Input::get('registrationNumber') )->first();
		if ($validator->fails() && count($car) != 1) {
		
			$messages = $validator->messages();
			return Redirect::to('cars/edit/'.Input::get('id'))->withErrors($validator);
			
		} else {
			DB::table('cars')->where('id', Input::get('id'))->update(
				array(
						'driver_id' => Input::get('driver_id'),
						'registrationNumber' => Input::get('registrationNumber'),
						'dateOilChange' => Input::get('dateOilChange'),
						'dateService' => Input::get('dateService')
					)
			);
			return Redirect::action('CarsController@index');
		}
    }

    public function delete($id)
    {
        $car = DB::table('cars')->where('id', $id)->first();
		$driver = DB::table('drivers')->where('id', $car->driver_id)->first();
        return View::make('car.delete', array('car' => $car, 'driver' => $driver));
    }

    public function handleDelete()
    {
        DB::table('cars')->where('id', Input::get('id'))->delete();
		return Redirect::action('CarsController@index');
    }
}