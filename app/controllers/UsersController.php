<?php

// app/controllers/UsersController.php

class UsersController extends BaseController
{
    /*
     * Function generates the view to create a new user.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('user.create');
    }

    /*
     * Function creates new user.
     *
     * @return Response
     */
    public function handleCreate()
    {
        $rules = array(
            'email' => 'required|unique:users',
            'password' => 'required'
        );

        $messages = array(
            'email.required' => 'Pole email jest wymagane.',
            'email.unique' => 'Proszę podać inny email.',
            'password.required' => 'Pole hasło jest wymagane.'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {

            $messages = $validator->messages();
            return Redirect::to('user/create')->withErrors($validator);

        } else {

            $user = new User;
            $user->password = Hash::make(Input::get('password'));
            $user->email = Input::get('email');
            $user->save();

            return Redirect::to('user/login');
        }
    }
    /*
     * Function generate a view to log in.
     *
     * @return Response
     */
    public function login()
    {
        if (Auth::check())
        {
            return Redirect::intended('/');
        }
        return View::make('user.login');
    }

    /*
     * Function allows to log on.
     *
     * @return Response
     */
    public function handleLogin()
    {
        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );

        $messages = array(
            'email.required' => 'Pole email jest wymagane.',
            'password.required' => 'Pole hasło jest wymagane.'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {

            $messages = $validator->messages();
            return Redirect::to('user/login')->withErrors($validator);

        } else {

            $credentials = Input::only('email', 'password');
            if (Auth::attempt($credentials)) {
                return Redirect::intended('/drivers/index');
            }
            return Redirect::to('/user/login')->withErrors('Nieprawidłowe dane do logowania.');
        }
    }
    /*
     * Function allow to log out.
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();
        return View::make('user.logout');
    }

}