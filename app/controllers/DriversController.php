<?php

// app/controllers/DriversController.php

class DriversController extends BaseController
{
    public function index($column = 'name', $sorttype = 'asc')
    {
        $driverss = DB::table('drivers')->orderBy($column, $sorttype)->get();
        $drivers = json_decode(json_encode($driverss), true);
        return View::make('driver.index', compact('drivers'));
    }

    public function index2($column = 'name', $sorttype = 'asc')
    {

       /* $owner = new Role;
        $owner->name = 'Owner';
        $owner->save();

        $admin = new Role;
        $admin->name = 'Admin';
        $admin->save();

        $managePosts = new Permission;
        $managePosts->name = 'view_orders';
        $managePosts->display_name = 'Wyświetlanie zlecen';
        $managePosts->save();

        $manageUsers = new Permission;
        $manageUsers->name = 'add_orders';
        $manageUsers->display_name = 'Dodawanie zlecen';
        $manageUsers->save();

        $manageUsers = new Permission;
        $manageUsers->name = 'edit_orders';
        $manageUsers->display_name = 'Edytowanie zlecen';
        $manageUsers->save();

        $manageUsers = new Permission;
        $manageUsers->name = 'delete_orders';
        $manageUsers->display_name = 'Usuwanie zlecen';
        $manageUsers->save();*/

        $perm = Permission::where('name','=','view_orders')->first();
        $user = User::where('email','=','test@test.pl')->first();
        $role = Role::where('name','=','Admin')->first();

        //dodawanie uprawnien dla usera
        //$user->roles()->attach( 2 );


        $role->perms()->sync(array($perm->id,2)); //rola admin dostaje uprawnienia do wyswietlania zamowien
        //return Hash::make('secret');
        //return $user->can("view_orders").''.$user->can("add_orders");
        $options = array(
            'validate_all' => false,// | false (Default: false),
            'return_type' => 'array'// | array | both (Default: boolean)
        );
        print_r($user->ability('Admin,Owner', 'add_orders,view_orders,edit_orders', $options));

        return $user->hasRole("Owner")." ".$user->hasRole("Admin");
        //return $user->roles()->attach( $role->id );

        $id = '1';
        //$tmp = $
        $driverss = DB::table('drivers');//->where('id','=', $id)->orderBy($column, $sorttype)->get();
        //$driverss = DB::table('drivers')->where('id','=', $id)->get();
      //  Debugbar::error("Error!");
        $driverss->where('id','=',$id);
        $driverss = $driverss->toSql();
        //$driverss = $driverss->where('id',$id)->orderBy($column, $sorttype)->toSql();
        return $driverss;
        print_r($driverss);
        echo "</br>";
        $drivers = json_decode(json_encode($driverss));
        print_r($drivers);
        return View::make('driver.index2', compact('drivers'));
    }

    public function create()
    {
        return View::make('driver.create');
    }

    public function handleCreate()
    {
        $rules = array(
        'pesel'  => 'required|unique:drivers'
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {

            $messages = $validator->messages();
            return Redirect::to('drivers/create')->withErrors($validator);

        } else {
            DB::table('drivers')->insert(
                array(
                        'name' => Input::get('name'),
                        'surname' => Input::get('surname'),
                        'pesel' => Input::get('pesel')
                    )
            );
            return Redirect::action('DriversController@index');
        }
    }

    public function edit($id)
    {
        $driver = DB::table('drivers')->where('id', $id)->first();
        return View::make('driver.edit', array('driver' => $driver));
    }

    public function handleEdit()
    {
        $rules = array(
            'pesel'  => 'required|unique:drivers'
        );

        $validator = Validator::make(Input::all(), $rules);
        $driver = DB::table('drivers')->where('id', Input::get('id'))->where('pesel', Input::get('pesel') )->first();
        if ($validator->fails() && count($driver) != 1) {

            $messages = $validator->messages();
            return Redirect::to('drivers/edit/'.Input::get('id'))->withErrors($validator);

        } else {
            DB::table('drivers')->where('id', Input::get('id'))->update(
                array(
                        'name' => Input::get('name'),
                        'surname' => Input::get('surname'),
                        'pesel' => Input::get('pesel')
                    )
            );
            return Redirect::action('DriversController@index');
        }
    }

    public function delete($id)
    {
        $driver = DB::table('drivers')->where('id', $id)->first();
        return View::make('driver.delete', array('driver' => $driver));
    }

    public function handleDelete()
    {
        DB::table('drivers')->where('id', Input::get('id'))->delete();
        return Redirect::action('DriversController@index');
    }
}