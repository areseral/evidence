<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration {

	/**
	 * Run the migrations.
	 * ID, Numer rejestracyjny, Kierowca, Data Wymiany Oleju, Data Przeglądu.
	 * @return void
	 */
	public function up()
	{
		
	
		Schema::create('cars', function($table)
		{
			$table->bigIncrements('id')->unsigned();
			$table->bigInteger('driver_id')->unsigned();
			$table->string('registrationNumber')->unique();
			$table->date('dateOilChange')->nullable();
			$table->date('dateService');
		});
		
		Schema::table('cars', function($table)
		{
			$table->foreign('driver_id')->references('id')->on('drivers');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cars');
	}

}
