<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * ID, Imię, Nazwisko, PESEL
	 * @return void   
	 */
	public function up()
	{
		Schema::create('drivers', function($table)
		{
			$table->bigIncrements('id')->unsigned();
			$table->string('name');
			$table->string('surname');
			$table->bigInteger('pesel')->unsigned()->unique();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('drivers');
	}

}
