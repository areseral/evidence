<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Driverstruckssemitrailers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	Schema::create('trucks', function($table)
        {
            $table->increments('id');
            $table->string('brand'); //marka /typ
            $table->string('vin'); //vin
            $table->date('vintage_car'); //rocznik
            $table->date('date_of_first_registration'); //data pierwszej rejestracji
            $table->string('fuel_dkv_card_number'); //karta paliwowa DKV-numer
            $table->date('validity_dkv_card'); //termin waznoci karty DKV
            $table->string('orlen_card'); //karta paliwowa orlen
            $table->date('validity_date_orlen_card'); //Termin ważności karty ORLEN,
            $table->date('validity_date_certificate_adr'); //Termin ważności certyfikatu ADR,
            $table->date('validity_insurance_oc'); //Termin ważności ubezpieczenia OC,
            $table->date('validity_insurance_ac'); //Termin ważności ubezpieczenia AC,
            $table->date('validity_tachograph'); //Data ważności tachografu,
            $table->date('validity_certificate_l'); //Termin ważności certyfikatu L,
            $table->date('validity_fire_extinguisher'); //Data ważności gaśnicy,
            $table->string('company'); //Spółka,
            $table->string('department_name_assigned_car'); //Nazwa działu, do którego jest przypisany samochód,
            $table->string('sub_dept_name'); //Poddział,
            $table->integer('actual_mileage'); //Aktualny przebieg,
            $table->integer('previos_mileage_oil_change'); //Przebieg poprzedniej wymiany oleju,
            $table->date('date_previos_oil_change'); //Pole Data poprzedniej daty wymiany oleju,
            $table->date('date_next_oil_change'); //Data następnej wymiany oleju,
            $table->date('date_vehicle_warranty'); //Data gwarancji pojazdu,
            $table->string('equipment'); //Wyposażenie,
            $table->string('note'); //Uwagi,
            $table->float('amount_of_depreciation'); //Kwota amortyzacji,
            $table->string('number_leasing_agreement'); //Numer umowy leasingowej,
            $table->string('number_of_asset'); //Numer ewidencyjny środka trwałego,
            $table->float('taxes_on_transport'); //Podatki od środka transportu.

            $table->string('name'); //nazwa
            $table->string('registration_number'); //rejestracja
            $table->integer('own'); //wlasne
        });

        Schema::create('semitrailers', function($table)
        {
            $table->increments('id');
            $table->string('company'); //Spółka,
            $table->string('department_name_assigned_car'); //Nazwa działu, do którego jest przypisany samochód,
            $table->string('sub_dept_name'); //Poddział,
            $table->string('brand'); //marka
            $table->string('type'); //Typ/Model:
            $table->string('vin'); //Nr VIN,
            $table->date('vintage_car'); //Rocznik
            $table->date('date_of_first_registration'); //Data pierwszej rejestracji
            $table->integer('number_compartment'); //Liczba komór cysterny,
            $table->date('validity_certificate_adr'); //Termin ważności certyfikatu ADR,
            $table->date('validity_insurance_oc'); //Termin ważności ubezpieczenia OC,
            $table->date('validity_insurance_ac'); //Termin ważności ubezpieczenia AC,
            $table->date('date_certificate_atp'); //Data certyfikatu ATP,
            $table->string('brand_generate'); //Marka\Typ Agregatu,
            $table->date('validity_date_generator'); //Data przeglądu agregatu,
            $table->date('validity_generator'); //Przegląd agregatu,
            $table->date('validity_indirect_research_tdt'); //Badania TDT dla badań pośrednich,
            $table->date('validity_periodic_research_tdt'); //Badania TDT dla badań okresowych,
            $table->date('validity_axis'); //Przegląd osi,
            $table->string('number_generator_carrier'); //Nr agregatu Carrier,
            $table->string('number_recorder'); //Nr rejestratora,
            $table->date('validity_fire_extinguisher'); //Data przeglądu gaśnicy,
            $table->date('date_vehicle_warranty'); //Data gwarancji pojazdu,
            $table->string('equipment'); //Wyposażenie,
            $table->string('note'); //Uwagi,
            $table->float('amount_of_depreciation'); //Kwota amortyzacji,
            $table->string('number_leasing_agreement'); //Numer umowy leasingowej,
            $table->string('number_of_asset'); //Numer ewidencyjny środka trwałego,
            $table->float('taxes_on_transport'); //Podatki od środka transportu.
            //****** pobrane z modelu
            $table->string('name'); //nazwa
            $table->string('registration_number'); //rejestracja
            $table->integer('own'); //wlasne
        });

        Schema::create('containers', function($table)
        {
            $table->increments('id');
            $table->string('company'); //Spółka,
            $table->string('department_name_assigned_car'); //Nazwa działu, do którego jest przypisany kontener,
            $table->string('sub_dept_name'); //Poddział,
            $table->string('type'); //'Typ kontenera,
            $table->string('number_container'); //Nr kontenera,
            $table->string('owner_container'); //Właściciel kontenera,
            $table->string('number_of_asset'); //Numer ewidencyjny środka trwałego,
            $table->date('validity_indirect_research_tdt'); //Badania TDT (VERITAS) dla badań pośrednich,
            $table->date('validity_periodic_research_tdt'); //Badania TDT (VERITAS) dla badań okresowych,
            $table->string('note'); //Uwagi.
        });

        Schema::create('drivers', function($table)
        {
            $table->increments('id');
            $table->string('company'); //spolka
            $table->string('dept_name'); //nazwa dzialu
            $table->string('sub_dept_name'); //poddział
            $table->string('name'); //imie
            $table->string('surname'); //nazwisko
            $table->integer('pesel')->unsigned()->unique(); //nr pesel
            $table->string('identity_card'); //dowod osobisty
            $table->date('validity_identity_card'); //waznosc dowodu osobistego
            $table->string('passport'); //paszport
            $table->date('validity_passport'); //waznosc paszportu
            $table->string('credit_card'); //karta platnicza MasterCard
            $table->date('validity_credit_card'); //termin ważności karty MasterCard
            $table->date('validity_adr_basic'); //termin waznosci certyfikatu ADR - podstawowy
            $table->date('validity_adr_tank'); //termin ważności certyfikatu ADR - cysterny
            $table->date('validity_driving_license'); //Ważność prawo jazdy
            $table->string('number_card_driver'); //nr karty kierowcy - walidacja do karty kierowcy
            $table->date('validate_card_driver'); //ważność karty kierowcy
            $table->boolean('course_carriage_of_goods'); //kurs przewozu rzeczy
            $table->boolean('certificate_a_clean_criminal_record'); //zaswiadczenie o niekaralności
            $table->date('medical_certificate'); //zaświadczenie lekarskie(termin)
            $table->boolean('health_certificate'); //książeczka zdrowia
            $table->date('date_health_certificate'); //data ksiazeczki zdrowia
            $table->string('buisness_telephone_number'); //telefon służbowy
            $table->string('private_telephone_number'); //telefon prywatny
            $table->string('email'); //E-mail
            $table->string('note'); //uwagi
            //****** pobrane z modelu
            $table->integer('semitrailer_id'); //naczepa id
            $table->integer('truck_id'); //ciagnik id
            $table->integer('carrier_id'); //przewoznik id
			$table->foreign('semitrailer_id')->references('id')->on('semitrailers');
            $table->foreign('truck_id')->references('id')->on('trucks');
            $table->foreign('carrier_id')->references('id')->on('carriers');
			
        });
	
      /* Schema::table('drivers', function($table)
       {
            $table->foreign('semitrailer_id')->references('id')->on('semitrailers');
            $table->foreign('truck_id')->references('id')->on('trucks');
            $table->foreign('carrier_id')->references('id')->on('carriers');
        });*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('truck');
		Schema::drop('semitrailers');
		Schema::drop('containers');
		Schema::drop('drivers');
	}

}
