@extends('layout')

@section('content')
Edycja

	<form action="{{ action('CarsController@handleEdit') }}" method="post" role="form">
	@if ($errors->has())
			<p class="alert alert-error">
				@foreach ($errors->all() as $error)
					{{ $error }}		
				@endforeach
			</p>
		@endif
	<input type="hidden" name="id" value="{{ $car->id }}">
		<div class="form-group">
            <label for="">Kierowca</label>
           <select name="driver_id">
				@foreach ($drivers as $driver)
					<option value="{{$driver->id}}" @if($driver->id == $car->driver_id) selected @endif>{{$driver->name}} {{$driver->surname}} PESEL: {{$driver->pesel}}</option>		
				@endforeach
			</select>
        </div>
        <div class="form-group">
            <label for="registrationNumber">Numer rejestracyjny</label>
            <input type="text" class="form-control" name="registrationNumber" value="{{ $car->registrationNumber }}"/>
        </div>
        <div class="form-group">
            <label for="dateOilChange">Data wymiany oleju</label>
            <input type="date" class="form-control" name="dateOilChange" value="{{  $car->dateOilChange }}"/>
        </div>
		<div class="form-group">
            <label for="dateService">Data przeglądu</label>
            <input type="date" class="form-control" name="dateService" value="{{  $car->dateService }}"/>
        </div>
        <input type="submit" value="Zapisz" class="btn btn-primary" />
        <a href="{{ action('CarsController@index') }}" class="btn">Cancel</a>
    </form>
@stop