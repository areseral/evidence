@extends('layout')

@section('content')

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Auta</title>
   
</head>
<body>
   Auta
   
    @if(count($cars) == 0)
        <p>Brak rekordów do wyświetlenia</p>
    @else
	
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Kierowca <a href="{{ URL::to('cars/index/name/asc')}}" >&#x25B2;</a> <a href="{{ URL::to('cars/index/name/desc') }}" >&#x25BC;</a></th>
                    <th>Numer rejestracyjny <a href="{{ URL::to('cars/index/registrationNumber/asc')}}" >&#x25B2;</a> <a href="{{ URL::to('cars/index/registrationNumber/desc') }}" >&#x25BC;</a></th>
                    <th>Data wymiany oleju <a href="{{ URL::to('cars/index/dateOilChange/asc')}}" >&#x25B2;</a> <a href="{{ URL::to('cars/index/dateOilChange/desc') }}" >&#x25BC;</a></th>
                    <th>Data przeglądu <a href="{{ URL::to('cars/index/dateService/asc')}}" >&#x25B2;</a> <a href="{{ URL::to('cars/index/dateService/desc') }}" >&#x25BC;</a></th>
					<th>Akcje</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cars as $car)
					
					<tr>
						<td>{{ $car['name'] }} {{ $car['surname'] }}</td>
						<td>{{ $car['registrationNumber'] }}</td>
						<td>{{ $car['dateOilChange'] }}</td>
						<td>{{ $car['dateService'] }}</td>
						<td>
							<a class="btn" href="{{ action('CarsController@edit', $car['id'] ) }}" >Edycja</a>
							<a class="btn btn-danger" href="{{ action('CarsController@delete', $car['id'] ) }}" >Usuń</a>
						</td>
					</tr>
					
                @endforeach
            </tbody>
        </table>
    @endif
	<a class="btn btn-success" href="{{ action('CarsController@create') }}" >Dodaj nowe auto</a>
</body>
</html>
@stop