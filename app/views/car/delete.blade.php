﻿@extends('layout')

@section('content')
	Usuwanie pojazdu
	
		<form  action="{{ action('CarsController@handleDelete') }}" method="post" role="form">
			<input type="hidden" name="id" value="{{ $car->id }}">
			<div class="form-group">
				<label class="control-label" for="">Kierowca</label>
				<input type="text" name="driver_id" value="{{ $driver->name }} {{ $driver->surname }} PESEL: {{ $driver->pesel }}" disabled/>
			</div>
			<div class="form-group">
				<label class="control-label" for="registrationNumber">Numer rejestracyjny</label>
				<input type="text" class="help-inline" name="registrationNumber" value="{{ $car->registrationNumber }}" disabled/>
			</div>
			<div class="form-group">
				<label class="control-label" for="dateOilChange">Data wymiany oleju</label>
				<input type="date" class="help-inline" name="dateOilChange" value="{{  $car->dateOilChange }}" disabled/>
			</div>
			<div class="form-group">
				<label class="control-label" for="dateService">Data przeglądu</label>
				<input type="date" class="help-inline" name="dateService" value="{{  $car->dateService }}" disabled/>
			</div>
			<input type="submit" value="Usun" class="btn btn-primary" />
			<a class="btn" href="{{ action('CarsController@index') }}" class="btn">Cancel</a>
		</form>
	
@stop