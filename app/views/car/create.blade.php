@extends('layout')

@section('content')
	Dodawanie nowego auta
	
    <form action="{{ action('CarsController@handleCreate') }}" method="post" role="form">
		@if ($errors->has())
			<p class="alert alert-error">
				@foreach ($errors->all() as $error)
					{{ $error }}		
				@endforeach
			</p>
		@endif
		<div class="form-group">
            <label for="">Kierowca</label>
			<select name="driver_id">
				@foreach ($drivers as $driver)
					<option value="{{$driver->id}}">{{$driver->name}} {{$driver->surname}} PESEL: {{$driver->pesel}}</option>		
				@endforeach
			</select>
        </div>
        <div class="form-group">
            <label for="registrationNumber">Numer rejestracyjny</label>
            <input type="text" class="form-control" name="registrationNumber" />
        </div>
        <div class="form-group">
            <label for="dateOilChange">Data wymiany oleju</label>
            <input type="date" class="form-control" name="dateOilChange" />
        </div>
		<div class="form-group">
            <label for="dateService">Data przeglądu</label>
            <input type="date" class="form-control" name="dateService"/>
        </div>
        <input type="submit" value="Utwórz" class="btn btn-primary" />
        <a href="{{ action('CarsController@index') }}" class="btn">Cancel</a>
    </form>
	
@stop