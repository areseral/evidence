<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">.
	{{ HTML::style('css/bootstrap.min.css'); }}
</head>
    <body>
		<div class="container">
			<h1>Ewidencja</h1>
		@if (Auth::check())
		     <a href="{{ URL::to('user/logout')}}" >Wyloguj się</a>
		@else
		     <a href="{{ URL::to('user/login')}}" >Zaloguj się</a>
		@endif
			<h4><a href="{{ URL::to('cars/index')}}" >Auta</a>   <a href="{{ URL::to('drivers/index')}}" >Kierowcy</a></h4>
			@yield('content')
		</div>
    </body>
</html>