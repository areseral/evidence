@extends('layout')

@section('content')

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kierowcy</title>
   
</head>
<body>
   Kierowcy
       
    @if (count($drivers)==0)
        <p>There are no drivers! :(</p>
    @else
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Name <a href="{{ URL::to('drivers/index/name/asc')}}" >&#x25B2;</a> <a href="{{ URL::to('drivers/index/name/desc') }}" >&#x25BC;</a></th>
                    <th>Surname <a href="{{ URL::to('drivers/index/surname/asc')}}" >&#x25B2;</a> <a href="{{ URL::to('drivers/index/surname/desc') }}" >&#x25BC;</a></th>
                    <th>PESEL <a href="{{ URL::to('drivers/index/pesel/asc')}}" >&#x25B2;</a> <a href="{{ URL::to('drivers/index/pesel/desc') }}" >&#x25BC;</a></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($drivers as $drive)
                <tr>
                    <td>{{ $drive['name'] }}</td>
                    <td>{{ $drive['surname'] }}</td>
                    <td>{{ $drive['pesel'] }}</td>
                    <td>
                        <a class="btn" href="{{ action('DriversController@edit', $drive['id'] ) }}" >Edycja</a>
                        <a class="btn btn-danger" href="{{ action('DriversController@delete', $drive['id'] ) }}" >Usuń</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
	<a class="btn btn-success" href="{{ action('DriversController@create' ) }}" >Dodaj nowego kierowcę</a>
</body>
</html>
@stop