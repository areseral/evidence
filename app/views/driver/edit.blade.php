@extends('layout')

@section('content')
Edycja

	<form action="{{ action('DriversController@handleEdit') }}" method="post" role="form">
		@if ($errors->has())
			<p class="alert alert-error">
				@foreach ($errors->all() as $error)
					{{ $error }}		
				@endforeach
			</p>
		@endif
	<input type="hidden" name="id" value="{{ $driver->id }}">
        <div class="form-group">
            <label for="name">Imię</label>
            <input type="text" class="form-control" name="name" value="{{ $driver->name}}"/>
        </div>
        <div class="form-group">
            <label for="surname">Nazwisko</label>
            <input type="text" class="form-control" name="surname" value="{{ $driver->surname}}"/>
        </div>
		<div class="form-group">
            <label for="pesel">PESEL</label>
            <input type="text" class="form-control" name="pesel" value="{{ $driver->pesel}}"/>
        </div>
        
        <input type="submit" value="Zapisz" class="btn btn-primary" />
        <a href="{{ action('DriversController@index') }}" class="btn">Cancel</a>
    </form>
@stop