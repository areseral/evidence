﻿@extends('layout')

@section('content')
	Usuwanie kierowcy
	

   <form action="{{ action('DriversController@handleDelete') }}" method="post" role="form">
	<input type="hidden" name="id" value="{{ $driver->id }}">
        <div class="form-group">
            <label for="name">Imię</label>
            <input type="text" class="form-control" name="name" value="{{ $driver->name}}" disabled/>
        </div>
        <div class="form-group">
            <label for="surname">Nazwisko</label>
            <input type="text" class="form-control" name="surname" value="{{ $driver->surname}}" disabled/>
        </div>
		<div class="form-group">
            <label for="pesel">PESEL</label>
            <input type="text" class="form-control" name="pesel" value="{{ $driver->pesel}}" disabled/>
        </div>
        
        <input type="submit" value="Usuń" class="btn btn-primary" />
        <a href="{{ action('DriversController@index') }}" class="btn">Cancel</a>
    </form>
	
@stop