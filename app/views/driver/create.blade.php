@extends('layout')

@section('content')
	Tworzenie kierowcy
	
    <form action="{{ action('DriversController@handleCreate') }}" method="post" role="form">
		@if ($errors->has())
			<p class="alert alert-error">
				@foreach ($errors->all() as $error)
					{{ $error }}		
				@endforeach
			</p>
		@endif
        <div class="form-group">
            <label for="name">Imię</label>
            <input type="text" class="form-control" name="name" />
        </div>
        <div class="form-group">
            <label for="surname">Nazwisko</label>
            <input type="text" class="form-control" name="surname" />
        </div>
		<div class="form-group">
            <label for="pesel">PESEL</label>
            <input type="text" class="form-control" name="pesel" />
        </div>
        
        <input type="submit" value="Utwórz" class="btn btn-primary" />
        <a href="{{ action('DriversController@index') }}" class="btn">Cancel</a>
    </form>
	
@stop