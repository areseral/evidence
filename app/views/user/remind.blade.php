@extends('layout')

@section('content')

<h2>Przypominanie hasla</h2>

@if (Session::get('status') != "")
			<p class="alert alert-error">

					{{ Session::get('status') }}
			</p>
@endif

<form action="{{ action('RemindersController@postRemind') }}" method="POST">
    <input type="text" name="email">
    <input type="submit" value="Send Reminder">
</form>

 @stop