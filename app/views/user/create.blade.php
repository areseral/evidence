@extends('layout')

@section('content')

<h2>Rejestracja</h2>
@if ($errors->has())
			<p class="alert alert-error">
				@foreach ($errors->all() as $error)
					{{ $error }}
				@endforeach
			</p>
@endif

<form action="{{ action('UsersController@handleCreate') }}" method="post">
<p><label for="email">Email:</label></p>
<p><input type="text" name="email" placeholder="Email" /></p>
<p><label for="password">Password:</label></p>
<p><input type="password" name="password" placeholder="Password" /></p>
<p><input type="submit" value="Create" /></p>
</form>


 @stop