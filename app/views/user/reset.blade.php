@extends('layout')

@section('content')

<h2>Resetowanie hasła</h2>

@if (Session::get('error') != "")
    <p class="alert alert-error">
        {{ Session::get('error') }}
    </p>
@endif

<form action="{{ action('RemindersController@postReset') }}" method="POST">
    <input type="hidden" name="token" value="{{ $token }}">
    <p><label for="email">Email:</label></p>
    <input type="email" name="email">
    <p><label for="pass">Password:</label></p>
    <input type="password" name="password">
    <p><label for="password_confirm">Confirm password</label></p>
    <input type="password" name="password_confirmation">
    <input type="submit" value="Reset Password">
</form>

@stop