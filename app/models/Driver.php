<?php

use Zizaco\Entrust\HasRole;

class Driver extends Eloquent 
{
    use HasRole;
    protected $table = 'drivers';
}