<?php

return array(


    'driver' => 'smtp',

    'host' => 'smtp.gmail.com',

    'port' => 587,

    'from' => array('address' => 'test@gmail.com', 'name' => 'Przypomnienie hasła'),

    'encryption' => 'tls',

    'username' => 'pass@gmail.com',

    'password' => 'pass',

    'sendmail' => '/usr/sbin/sendmail -bs',

    'pretend' => false,

);
