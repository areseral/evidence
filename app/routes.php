<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::model('driver', 'Driver', function()
{
    throw new NotFoundHttpException;
});

Route::model('car', 'Car', function()
{
    throw new NotFoundHttpException;
});

Route::model('user', 'User', function()
{
    throw new NotFoundHttpException;
});

Route::get('/', function()
{
    return View::make('hello');
});

Route::get('evidence', function()
{
    $drivers = Driver::all();
    return View::make('evidence')->with('drivers', $drivers);
});

Route::get('/user/create', 'UsersController@create');
Route::post('/user/create', 'UsersController@handleCreate');


Route::get('/user/login', 'UsersController@login');
Route::post('/user/login', 'UsersController@handleLogin');
Route::get('/user/logout', 'UsersController@logout');

Route::post('/user/remind', 'RemindersController@postRemind');
Route::get('/user/remind', 'RemindersController@getRemind');
Route::get('/password/reset/{token?}', 'RemindersController@getReset');
Route::post('/user/reset', 'RemindersController@postReset');

Route::get('/drivers/index/{column?}/{sorttype?}', 'DriversController@index');
Route::get('/drivers/edit/{id}', 'DriversController@edit');
Route::get('/drivers/delete/{id}', 'DriversController@delete');
Route::get('/drivers/create', 'DriversController@create');

Route::get('/drivers/index2',array(
    'before' => 'auth',
    function()
    {
        return App::make('DriversController')->index2();
    })
);

Route::post('/drivers/create', 'DriversController@handleCreate');
Route::post('/drivers/edit', 'DriversController@handleEdit');
Route::post('/drivers/delete', 'DriversController@handleDelete');

Route::get('/cars/index/{column?}/{sorttype?}', 'CarsController@index');
Route::get('/cars/edit/{id}', 'CarsController@edit');
Route::get('/cars/delete/{id}', 'CarsController@delete');
Route::get('/cars/create', 'CarsController@create');

Route::post('/cars/create', 'CarsController@handleCreate');
Route::post('/cars/edit', 'CarsController@handleEdit');
Route::post('/cars/delete', 'CarsController@handleDelete');
